﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please Enter A Number: ");
            int number = int.Parse(Console.ReadLine());

            if ((number % 2) == 0)
            {
                Console.WriteLine(number + " is an Even Number");
            }
            else
            {
                Console.WriteLine(number + " is an Odd Number");
            }
            Console.WriteLine();
        }
    }
}

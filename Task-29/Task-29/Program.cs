﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program
    {
        static void Main(string[] args)
        {
            string words = "The quick brown fox jumped over the fence";
            char[] space = new char[] { ' ' };

            int wordcount = words.Split(space, StringSplitOptions.RemoveEmptyEntries).Length;

            Console.WriteLine();
            Console.WriteLine(words);
            Console.WriteLine();
            Console.WriteLine($"There are {wordcount} words in this string");
            Console.WriteLine();
        }
    }
}

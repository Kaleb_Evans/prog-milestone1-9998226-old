﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {                      
            Console.WriteLine();
            Console.WriteLine("Hello,");
            Console.WriteLine();
            Console.WriteLine("Please enter 5 seperate numbers pressing ENTER after each submission.");
            Console.WriteLine("your numbers will then be added together.");
            Console.WriteLine(); 

            var n = new List<int> {  };

            int a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());
            var c = int.Parse(Console.ReadLine());
            var d = int.Parse(Console.ReadLine());
            var e = int.Parse(Console.ReadLine());

            n.Add(a);
            n.Add(b);
            n.Add(c);
            n.Add(d);
            n.Add(e);

            var output = (a + b + c + d + e);

            Console.WriteLine($"{a}+{b}+{c}+{d}+{e}={output}");
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = "Monday Tuesday Wednesday Thursday Friday Saturday Sunday";
            var number = "1 2 3 4 5 6 7";

            var output = days.Split(' ');
            var output2 = number.Split(' ');

            Console.WriteLine();
            Console.WriteLine($"{output[0]} is the {output2[0]}st day in the week");
            Console.WriteLine();
            Console.WriteLine($"{output[1]} is the {output2[1]}nd day in the week");
            Console.WriteLine();
            Console.WriteLine($"{output[2]} is the {output2[2]}rd day in the week");
            Console.WriteLine();
            Console.WriteLine($"{output[3]} is the {output2[3]}th day in the week");
            Console.WriteLine();
            Console.WriteLine($"{output[4]} is the {output2[4]}th day in the week");
            Console.WriteLine();
            Console.WriteLine($"{output[5]} is the {output2[5]}th day in the week");
            Console.WriteLine();
            Console.WriteLine($"{output[6]} is the {output2[6]}th day in the week");
            Console.WriteLine();
            
            
        }
    }
}

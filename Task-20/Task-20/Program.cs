﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[10] { 1, 2, 3, 4 ,5 ,6 ,7 ,8 ,9, 10 };

            int[] odd = a.Where((value, index) => index % 2 == 0).ToArray();

            Console.WriteLine(string.Join(", ", odd));

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;

            Console.WriteLine();
            Console.WriteLine("Hello,");
            Console.WriteLine();
            Console.WriteLine("Please enter a word.");
            Console.WriteLine();

            var word = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine($"The word {word} has {word.Length} characters " ); 
        }
    }
}

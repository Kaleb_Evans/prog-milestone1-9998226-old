﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine("Hello, please enter three numbers pressing ENTER after each submission");
            Console.WriteLine();

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine($"{c} + {a} + {b} = {c + b + a}");
            Console.WriteLine();
            Console.WriteLine($"({a} x {b}) / {c} = {(a * b) / c}");
            Console.WriteLine();
            Console.WriteLine($"({b} / {a}) + {c} = {(b / a) + c}");
            Console.WriteLine();
            Console.WriteLine($"({c} + {a}) - {b} = {(c + a) - b}");
            Console.WriteLine();
            Console.WriteLine($"({a} + {b}) x {c} = {(a + b) * c}");
            Console.WriteLine();
            Console.WriteLine();

        }
    }
}

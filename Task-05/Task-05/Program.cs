﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello.");
            Console.WriteLine("Please enter a 24hr time (0000 format) to be converted into 12hr time");

            var hr = Convert.ToInt32(Console.ReadLine());

            if (hr == 0000)
            {
                Console.WriteLine("It is 12:00 a.m. or Midnight. :)");
            }

            else if (hr == 0100)
            {
                Console.WriteLine("It is 1:00 a.m. ");
            }

            else if (hr == 0200)
            {
                Console.WriteLine("It is 2:00 a.m. ");
            }

            else if (hr == 0300)
            {
                Console.WriteLine("It is 3:00 a.m. ");
            }

            else if (hr == 0400)
            {
                Console.WriteLine("It is 4:00 a.m. ");
            }

            else if (hr == 0500)
            {
                Console.WriteLine("It is 5:00 a.m. ");
            }

            else if (hr == 0600)
            {
                Console.WriteLine("It is 6:00 a.m. ");
            }

            else if (hr == 0700)
            {
                Console.WriteLine("It is 7:00 a.m. ");
            }

            else if (hr == 0800)
            {
                Console.WriteLine("It is 8:00 a.m. ");
            }

            else if (hr == 0900)
            {
                Console.WriteLine("It is 9:00 a.m. ");
            }

            else if (hr == 1000)
            {
                Console.WriteLine("It is 10:00 a.m. ");
            }

            else if (hr == 1100)
            {
                Console.WriteLine("It is 11:00 a.m. ");
            }

            else if (hr == 1200)
            {
                Console.WriteLine("It is 12:00 a.m. ");
            }

            else if (hr == 1300)
            {
                Console.WriteLine("It is 1:00 p.m. ");
            }

            else if (hr == 1400)
            {
                Console.WriteLine("It is 2:00 p.m. ");
            }

            else if (hr == 1500)
            {
                Console.WriteLine("It is 3:00 p.m. ");
            }
            else if (hr == 1600)

            {
                Console.WriteLine("It is 4:00 p.m. ");
            }
            else if (hr == 1700)

            {
                Console.WriteLine("It is 5:00 p.m. ");
            }
            else if (hr == 1800)

            {
                Console.WriteLine("It is 6:00 p.m. ");
            }
            else if (hr == 1900)

            {
                Console.WriteLine("It is 7:00 p.m. ");
            }

            else if (hr == 2000)
            {
                Console.WriteLine("It is 8:00 p.m. ");
            }

            else if (hr == 2100)
            {
                Console.WriteLine("It is 9:00 p.m. ");
            }

            else if (hr == 2200)
            {
                Console.WriteLine("It is 10:00 p.m. ");
            }

            else if (hr == 2300)
            {
                Console.WriteLine("It is 11:00 p.m. ");
            }

            else if (hr == 2400)
            {
                Console.WriteLine("It is 12:00 p.m. or Midnight. :) ");
            }
          

        }
    }
}

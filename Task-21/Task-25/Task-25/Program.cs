﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {

            // Ask user for a number.

            Console.WriteLine("Hello, ");
            Console.WriteLine();
            Console.WriteLine("Please enter a number.");
            Console.WriteLine();

            // Try to get a number from input

            try
            {


                int input = int.Parse(Console.ReadLine());
                var a = 0;
                bool value = int.TryParse($"{input}", out a);

                // If an integer then print.

                Console.WriteLine();
                Console.WriteLine("----------------------------------");
                Console.WriteLine();
                Console.WriteLine($"You typed in: {input}");
                Console.WriteLine();
                Console.WriteLine($"Did you enter a number? : {value}");
                Console.WriteLine();
            }

            // If user entered a word
             
            // Print error 

            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Please enter a number.");
                Console.WriteLine();
            }
            
        }
    }
}
